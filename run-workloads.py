#!/usr/bin/env python3
import os
import shlex
import subprocess
import time
from datetime import datetime

def run_simulation(name, batcmd, schedcmd, massif=False):
    batcmd_prefix = ""
    if massif:
        batcmd_prefix = f"""valgrind --tool=massif --time-unit=ms --massif-out-file='{base}/{name}/massif.out' """

    cmd = f'''robin \
    --output-dir='{base}/{name}' \
    --batcmd="{batcmd_prefix}{batcmd} -e '{base}/{name}/out'"
    --schedcmd="{schedcmd}"'''
    args = shlex.split(cmd)

    t0 = time.perf_counter_ns()
    subprocess.run(args)
    t1 = time.perf_counter_ns()
    print(f'took {(t1-t0)/1e9} s')

    if massif:
        cmd = f"massif-to-csv '{base}/{name}/massif.out' '{base}/{name}/massif.csv'"
        subprocess.run(shlex.split(cmd))

base = "./run/" + datetime.now().isoformat()
os.makedirs(base)

easy_schedcmd = "batsched -v easy_bf_fast --verbosity=silent 1>/dev/null 2>/dev/null"

for i in range(5):
    run_simulation(f"kth-{i}", "batsim --mmax-workload -p cluster512.xml -w kth_3month.json", easy_schedcmd)
for i in range(3):
    run_simulation(f"kth-massif-{i}", "batsim --mmax-workload -p cluster512.xml -w kth_3month.json", easy_schedcmd, True)
